package net.floodlightcontroller.multicast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class Config {

	protected static Logger log = LoggerFactory.getLogger(Config.class);
	
	private static String JAR_DIR = getJarDir();
	//zakladajac, ze odpalamy z folderu target
	//TODO: dodac zmienna srodowiskowa?
	private static String CONFIG_PATH = JAR_DIR + "/../../inne/config.json";
	private static Config config;
	
	private int defaultPortNumber = 9999;
	private int portNumber = -1;

	public int getPortNumber() {
		return portNumber == -1 ? defaultPortNumber : portNumber;
	}

	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	
	public static Config getConfig() {
		if (Config.config == null) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(Config.CONFIG_PATH));
				Config.config = new Gson().fromJson(br, Config.class);
			} catch (Exception e) {
				log.error("Failure reading JSON config file for receiver updates. Using defaults.", e);
				Config.config = new Config();
			}
		}
		return Config.config;
		
	}
	
	private static String getJarDir() {
		File f = new File(System.getProperty("java.class.path"));
		File dir = f.getAbsoluteFile().getParentFile();
		String path = dir.toString();
		return path;
	}
}
