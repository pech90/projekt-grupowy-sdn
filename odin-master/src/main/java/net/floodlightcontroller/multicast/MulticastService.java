package net.floodlightcontroller.multicast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.linkdiscovery.LinkTuple;
import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.multicast.ReceiverUpdate.IpMacPair;
import net.floodlightcontroller.devicemanager.IDeviceService;

public class MulticastService {
	public static String handoffClientMAC;
	public static long handoffSwID;
		
	public static void saveHandoffClientMAC(String clientMAC) {
		handoffClientMAC = clientMAC;
	}

	public static void saveHandoffSwID(long switchID) {
		System.err.println("Zapisano switchID " + switchID);
		handoffSwID = switchID;
	}
	
	public static String currentServerMAC = "1c:6f:65:c1:e1:c8";
	public static List<IpMacPair> currentReceivers = new ArrayList<IpMacPair>();
	
	public static void saveMAC(List<IpMacPair> ipMac) {
		if(ipMac.size() > 0) {
			currentServerMAC = ipMac.get(0).getMac();
			System.out.println("Ustawiam MAC serwera na " + currentServerMAC);
		}
		else {
			System.err.println("Nieznany MAC. Ustawiam domyslny.");
			currentServerMAC = "00:00:00:00:00:00";
		}
	}
	
	public static void saveReceivers(List<IpMacPair> receivers) {
		currentReceivers = receivers;
	}
	
	private static List<Vertex> treeVerts;
	
	private static boolean debugMsgs = true;

	private static void dbg(String s) {
		if (debugMsgs)
			System.out.println(s);
	}

	public static void computeTopology(IFloodlightProviderService floodlightProvider,
			ILinkDiscoveryService linkDiscovery, IDeviceService deviceManager, String multicastAlgorithm) {

		long t0 = System.currentTimeMillis();
		Map<Long, IOFSwitch> switches = new HashMap<Long, IOFSwitch>(floodlightProvider.getSwitches());

		Map<LinkTuple, LinkInfo> myLinks;
		myLinks = new HashMap<LinkTuple, LinkInfo>(linkDiscovery.getLinks());
		Set<LinkTuple> linksBetweenSw = new HashSet<LinkTuple>(myLinks.keySet());

		ArrayList<IDevice> myDevices;
		myDevices = new ArrayList<IDevice>(deviceManager.getAllDevices());

		/* Handy map to keep IOFsw -> Vertex */
		Map<IOFSwitch, Vertex> sw2vert = new HashMap<IOFSwitch, Vertex>();
		List<Vertex> rxVerts = new ArrayList<Vertex>(); // only hosts
		List<Vertex> swVerts = new ArrayList<Vertex>(); // only switches

		/* Handle switches */
		for (IOFSwitch swi : switches.values()) {
			Vertex vert = new Vertex(swi);
			swVerts.add(vert);
			sw2vert.put(swi, vert);
		}

		for (Iterator<LinkTuple> iter = linksBetweenSw.iterator(); iter.hasNext();) {
			LinkTuple link = iter.next();
			IOFSwitch src = link.getSrc().getSw(), dst = link.getDst().getSw();
			Vertex srcVert = sw2vert.get(src), dstVert = sw2vert.get(dst);

			srcVert.adjacencies.add(new Edge(link, srcVert, dstVert, 1));
		}

		/* Handle clients */
		Set<String> clientMacs = new HashSet<>();
		for (IpMacPair ipMac : currentReceivers)
			clientMacs.add(ipMac.getMac());

		Vertex srvVert = null;

		for (IDevice dev : myDevices) {
			if (dev.getAttachmentPoints().length == 0)
				continue;

			long currentSwitchDPID = dev.getAttachmentPoints()[0].getSwitchDPID();
			IOFSwitch attachmentSw = switches.get(currentSwitchDPID);
			Vertex attachmentVert = sw2vert.get(attachmentSw);

			if (isHandoffClient(dev)) {
				System.err.println("Handoff " + dev.getMACAddress() + " from" + currentSwitchDPID + " to "
						+ handoffSwID);
				currentSwitchDPID = handoffSwID;
			}

			if (dev.getMACAddressString().equals(currentServerMAC)) {
				srvVert = new Vertex(dev);
				srvVert.adjacencies.add(new Edge(srvVert, attachmentVert, 1));
				attachmentVert.adjacencies.add(new Edge(attachmentVert, srvVert, 1));
			} else {
				if (!clientMacs.contains(dev.getMACAddressString())) {
					dbg("Skipping " + dev.getMACAddressString() + " - not a client");
					continue;
				}

				Vertex devVert = new Vertex(dev);

				rxVerts.add(devVert);
				devVert.adjacencies.add(new Edge(devVert, attachmentVert, 1));
				attachmentVert.adjacencies.add(new Edge(attachmentVert, devVert, 1));
			}
		}
		dbg("We got [" + swVerts.size() + " switches] [" + rxVerts.size() + " devices]");

		if (srvVert == null || srvVert.adjacencies.isEmpty())
			throw new IllegalArgumentException("Server not found or unconnected");

		List<Vertex> allVerts = new ArrayList<Vertex>();
		/* There might be an implicit dependency somewhere on server being the first on the list. */
		allVerts.add(srvVert);
		allVerts.addAll(rxVerts);
		allVerts.addAll(swVerts);

		Iterable<Edge> edgesList = new ArrayList<Edge>();

		if (multicastAlgorithm.equals("PPH")) {
			PPH newPPH = new PPH(allVerts, rxVerts, srvVert);
			edgesList = newPPH.edges();
		} else if (multicastAlgorithm.equals("STP")) {
			STP newSTP = new STP(allVerts, rxVerts, srvVert);
			edgesList = newSTP.edges();
		}

		for (Edge edge : edgesList)
			dbg("\"" + edge.source + "\"" + " -> " + "\"" + edge.target + "\"");

		long t1 = System.currentTimeMillis();

		if (treeVerts == null)
			treeVerts = new ArrayList<Vertex>();
		treeVerts.clear();

		/* Add server connection. */
		Vertex srvSwitchVert = srvVert.adjacencies.get(0).target;
		srvSwitchVert.inPort = (short) srvVert.dev.getAttachmentPoints()[0].getPort();
		treeVerts.add(srvSwitchVert);

		/* For every link b/w switches add inPort/outPort relations. */
		for (Edge e : edgesList) {
			Vertex s = e.source, d = e.target;
			LinkTuple link = e.link;

			if (link == null) /* It's a client or server link. */
				continue;

			s.outPorts.add(new OutObject(false, link.getSrc().getPort()));
			d.setInPort(link.getDst().getPort());

			treeVerts.add(d);
		}

		/* For every client add out rule to appropriate switch (AP) */
		for (IDevice dev : myDevices) {
			if (dev.getAttachmentPoints().length == 0)
				continue;
			if (!clientMacs.contains(dev.getMACAddressString()))
				continue;

			long currentSwitchDPID = dev.getAttachmentPoints()[0].getSwitchDPID();
			if (isHandoffClient(dev))
				currentSwitchDPID = handoffSwID;

			IOFSwitch apSwitch = switches.get(currentSwitchDPID);
			Vertex apVert = sw2vert.get(apSwitch);

			OutObject outObj = new OutObject(true, (short) dev.getAttachmentPoints()[0].getPort());
			outObj.setMacAddress(dev.getMACAddressString());
			outObj.setIpv4Address(dev.getIPv4Addresses());
			apVert.outPorts.add(outObj);
		}

		long t2 = System.currentTimeMillis();
		dbg("T First half: " + (t1 - t0) + " T Second half: " + (t2 - t1));
	}

	private static boolean isHandoffClient(IDevice dev) {
		if (handoffClientMAC == null)
			return false;
		String macString = dev.getMACAddressString();
		if (macString == null)
			return false;
		if (!macString.toLowerCase().equals(handoffClientMAC.toLowerCase()))
			return false;
		return true;
	}

	public static List<Vertex> getSwMulticastInfo() {
		if (currentReceivers.isEmpty()) {
			return Collections.<Vertex>emptyList();
		}
		
		return treeVerts;
	}
}
