package net.floodlightcontroller.multicast;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFType;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionDataLayerDestination;
import org.openflow.protocol.action.OFActionNetworkLayerDestination;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.util.HexString;
import org.openflow.util.U16;
import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.devicemanager.IDeviceService;
import net.floodlightcontroller.forwarding.Forwarding;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import net.floodlightcontroller.routing.IRoutingDecision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.InetAddresses;

public class MulticastForwarding extends Forwarding implements 
		IFloodlightModule {
	
	public static MulticastForwarding multicastForwarding = null;
	protected IFloodlightProviderService floodlightProvider;
	protected static Logger log;
	protected ILinkDiscoveryService linkDiscovery;
	protected IDeviceService deviceManager;

	@Override
	public String getName() {
	    return MulticastForwarding.class.getSimpleName();
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
	    Collection<Class<? extends IFloodlightService>> l =
	        new ArrayList<Class<? extends IFloodlightService>>();
	    l.add(IFloodlightProviderService.class);
	    return l;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {
		multicastForwarding = this;
	    floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
	    linkDiscovery = context.getServiceImpl(ILinkDiscoveryService.class);
	    deviceManager = context.getServiceImpl(IDeviceService.class);
	    log = LoggerFactory.getLogger(MulticastForwarding.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) {
	    floodlightProvider.addOFMessageListener(OFType.PACKET_IN, this);
	    ReceiverUpdateServer rus = new ReceiverUpdateServer();
        rus.run();
	}
	
    @Override
    public Command processPacketInMessage(IOFSwitch sw, OFPacketIn pi, IRoutingDecision decision, FloodlightContext cntx) {
        Ethernet eth = IFloodlightProviderService.bcStore.get(cntx, 
                                                       IFloodlightProviderService.CONTEXT_PI_PAYLOAD);
        if (eth.getEtherType() == Ethernet.TYPE_IPv4 && isMulticast((IPv4)eth.getPayload())){
        	doMulticast(sw, pi, cntx);
        }
        
        return Command.CONTINUE;
	}

	private static boolean debugMsgs = true;

	private static void dbg(String s) {
		if (debugMsgs)
			System.out.println(s);
	}

	private Boolean isMulticast(IPv4 ipPkt) {
		int dest = ipPkt.getDestinationAddress();
		InetAddress address = InetAddresses.fromInteger(dest);
		return address.isMulticastAddress();
	}

	private void rebuildMulticastFlows() {
		List<Vertex> vertices = null;
		int multicastIP = 1;
		String defaultMulticastGroup = "230.0.0.250";

		long start = System.nanoTime();

		try {
			multicastIP = InetAddresses.coerceToInteger(InetAddress.getByName(defaultMulticastGroup));
		} catch (Exception ex) {
			System.err.println("Bad multicast address: " + ex.toString());
		}

		try {
			MulticastService.computeTopology(floodlightProvider, linkDiscovery, deviceManager, "PPH");
			vertices = MulticastService.getSwMulticastInfo();
		} catch (Exception e) {
			System.err.println("Building mcast tree failed: " + e.toString());
			e.printStackTrace();
			return;
		}

		long topology = System.nanoTime();
		List<OFAction> actionList = new ArrayList<OFAction>();
		LinkedList<OFMessage> li = new LinkedList<OFMessage>();
		List<Long> updated = new LinkedList<>();

		for (Vertex vertex : vertices) {
			IOFSwitch swi = vertex.sw;
			int flowLength = OFFlowMod.MINIMUM_LENGTH;

			actionList.clear();
			li.clear();

			int wildcards = ((Integer)swi.getAttribute(IOFSwitch.PROP_FASTWILDCARDS)).intValue() &
						~OFMatch.OFPFW_IN_PORT &
						~OFMatch.OFPFW_DL_TYPE &
						~OFMatch.OFPFW_NW_DST_MASK;
			OFMatch match = new OFMatch()
					.setInputPort(vertex.inPort)
					.setNetworkDestination(multicastIP)
					.setDataLayerType((short)0x0800)
					.setWildcards(wildcards);
			OFFlowMod flow = new OFFlowMod()
					.setCookie(200)
					.setPriority((short) 200)
					.setMatch(match)
			//TODO: Ustawic krotszy timeout po testach
					.setIdleTimeout((short) 300)
			//TODO: Tutaj moze ustawiac jakis ID?
					.setBufferId(OFPacketOut.BUFFER_ID_NONE);

		    for (OutObject o : vertex.getOutPorts()) {
				if (vertex.getInPort() == o.getOutPort() || o.getOutPort() == 0 || vertex.getInPort() == 0) {
					System.err.println("Pomijam bledny port dla forward vertexa: " + vertex);
					continue;
				}
				if (o.isUser())
					flowLength += addClientAction(actionList, o);
				else
					flowLength += addSwitchAction(actionList, o);
			}

			flow.setActions(actionList);
			flow.setLength(U16.t(flowLength));

			li.add(flow);
			swi.clearAllFlowMods();

			if (actionList.size() > 0) {
		        dbg("Setting flow on a switch " + swi.getId());
				try {
					swi.write(li, null);
				} catch (Exception ex) {
					dbg("Error while writing forward flow: " + ex.toString());
				}
	        } else {
				log.info("No actions, skipping write for switch " + swi.getId());
			}
			updated.add(swi.getId());
		}

		long setting = System.nanoTime();

		Map<Long, IOFSwitch> switches = new HashMap<Long, IOFSwitch>(floodlightProvider.getSwitches());
		for (Map.Entry<Long, IOFSwitch> entry : switches.entrySet()) {
			IOFSwitch sw = entry.getValue();
			if (updated.contains(sw.getId()))
				continue;
			sw.clearAllFlowMods();
			dbg("Purge flows from switch " + sw.getId());
		}

		long done = System.nanoTime();

		log.info("Timing stats: " + (topology - start)/1000 + " + "
								  + (setting - topology)/1000 + " + "
								  + (done - setting)/1000 + "us");
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter("/stats", true));
			output.append("[us] " + (topology - start)/1000 + ", "
								  + (setting - topology)/1000 + ", "
								  + (done - setting)/1000 + ", "
								  + updated.size() + ", "
								  + switches.size() + ", "
								  + "\n");
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int addClientAction(List<OFAction> actionList, OutObject o) {
		OFActionDataLayerDestination macAction = new OFActionDataLayerDestination();
		OFActionNetworkLayerDestination ipAction = new OFActionNetworkLayerDestination();
		OFActionOutput outputAction = new OFActionOutput();

		byte[] clientMac = HexString.fromHexString(o.getMacAddress());
		macAction.setDataLayerAddress(clientMac);
		ipAction.setNetworkAddress(o.getIpv4Address()[0]);
		outputAction.setPort(o.getOutPort());

		macAction.setLength((short) OFActionDataLayerDestination.MINIMUM_LENGTH);
		ipAction.setLength((short) OFActionNetworkLayerDestination.MINIMUM_LENGTH);
		outputAction.setLength((short) OFActionOutput.MINIMUM_LENGTH);

		actionList.add(macAction);
		actionList.add(ipAction);
		actionList.add(outputAction);

		return OFActionDataLayerDestination.MINIMUM_LENGTH +
				OFActionNetworkLayerDestination.MINIMUM_LENGTH +
				OFActionOutput.MINIMUM_LENGTH;
	}

	private int addSwitchAction(List<OFAction> actionList, OutObject o) {
		OFActionOutput outputAction = new OFActionOutput().setPort(o.getOutPort());
		outputAction.setLength((short) OFActionOutput.MINIMUM_LENGTH);

		actionList.add(outputAction);
		return OFActionOutput.MINIMUM_LENGTH;
	}

	public void rebuildMulticastFlows(String reason) {
		log.info("Wywolano przebudowanie drzewa: " + reason);
		rebuildMulticastFlows();
	}

	protected void doMulticast(IOFSwitch sw, OFPacketIn pi, FloodlightContext cntx) {
		// Ignore
	}
}
