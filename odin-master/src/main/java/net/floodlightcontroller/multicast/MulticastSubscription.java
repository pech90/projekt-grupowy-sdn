package net.floodlightcontroller.multicast;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.openflow.protocol.OFPacketIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.forwarding.Forwarding;
import net.floodlightcontroller.multicast.ReceiverUpdate.IpMacPair;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.packet.IPv4;
import com.google.gson.Gson;

public class MulticastSubscription {

	protected static Logger log = LoggerFactory.getLogger(MulticastSubscription.class);
	
	public static boolean isSubscriptionListEmpty(int ipAddr) {
		return true;
	}
	
	public static Collection<IDevice> getSubscribers() {
		return Collections.emptyList();
	}
	
	
//	public static void parseJSON() {
//		//TODO: dla testow
//		String json = '{'
//		    + "'type': 'fullUpdate',"
//		    + "'groupIP': '230.0.0.255',"
//		    + "'receivers': ["
//		    + "    '10.1.1.103'"
//		    + "]"
//		+ "}";
//		try {
//			ReceiverUpdate update = new Gson().fromJson(json, ReceiverUpdate.class);
//			System.out.println(update);
//		} catch (Exception e){
//			log.error("Failure reading JSON file", e);
//		}
//	}
	
	public static void parseUpdate(String json) {
		try {
			ReceiverUpdate update = new Gson().fromJson(json, ReceiverUpdate.class);
			log.info("Received update " + update);
			if (update.getType().equals("serverSwitch")) {
				if (MulticastForwarding.multicastForwarding != null) {
					MulticastService.saveMAC(update.getData());
					System.out.println(update);
					MulticastForwarding.multicastForwarding.rebuildMulticastFlows("Przelaczenie serwera");
				}
			} else if (update.getType().equals("fullUpdate")) {
				List<IpMacPair> receivers = update.getData();
				MulticastService.saveReceivers(receivers);
				System.out.println(update);
				MulticastForwarding.multicastForwarding.rebuildMulticastFlows("Zmiana klientow");
			}
		} catch (Exception e){
			log.error("Failure reading JSON update", e);
			return;
		}
	}
}
