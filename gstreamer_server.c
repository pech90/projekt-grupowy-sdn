#include <gst/gst.h>

/* Structure to contain all our information, so we can pass it around */
typedef struct _CustomData {
	GstElement *pipeline;
	GstElement *videosrc, *timeoverlay, *ffmpegcolorspace, *x264enc, *rtph264pay, *udpsink;
	gboolean playing;      /* Are we in the PLAYING state? */
	gboolean terminate;    /* Should we terminate execution? */
	gint64 duration;       /* How long does this media last, in nanoseconds */
	int sock;
	GstClock *clock;
	GstClockID clk_id;
	unsigned long long time_diff;
} CustomData;

/* Forward definition of the message processing function */
static void handle_message (CustomData *data, GstMessage *msg);

#include <sys/time.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#define UDP_CONTROL_PORT 60001

gboolean clk_end_cb(GstClock *clock,
		    GstClockTime time,
		    GstClockID id,
		    gpointer user_data)
{
	CustomData *data = user_data;
	GstStateChangeReturn ret;

	ret = gst_element_set_state (data->pipeline, GST_STATE_NULL);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		g_printerr ("Unable to stop pipeline.\n");
		return TRUE;
	}

	gst_clock_id_unref (id);

	return FALSE;
}

gboolean clk_start_cb(GstClock *clock,
		      GstClockTime time,
		      GstClockID id,
		      gpointer user_data)
{
	CustomData *data = user_data;
	GstStateChangeReturn ret;

	/* Start playing */
	ret = gst_element_set_state (data->pipeline, GST_STATE_PLAYING);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		g_printerr ("Unable to set the pipeline to the playing state.\n");
		return TRUE;
	}

	gst_clock_id_unref (id);

	return FALSE;
}


int open_control_connection()
{
	int ret, sock;
	struct sockaddr_in servaddr;

	sock = ret = socket(AF_INET, SOCK_DGRAM, 0);
	if (ret < 0)
		return ret;

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(UDP_CONTROL_PORT);
	ret = bind(ret, (struct sockaddr *)&servaddr, sizeof(servaddr));
	if (ret < 0)
		return ret;

	return sock;
}

void serve_controller(CustomData *data)
{
	#define BUF_LEN 1024
	char buf[BUF_LEN];
	ssize_t ret;
	struct sockaddr_in src_addr;
	socklen_t src_len = sizeof(src_addr);

	/* Don't bother with loops, this will be called every 100ms. */

	ret = recvfrom(data->sock, buf, BUF_LEN, MSG_DONTWAIT,
		       (struct sockaddr *)&src_addr, &src_len);
	if (ret == -1) {
		if (errno != EAGAIN && errno != EWOULDBLOCK)
			g_printerr ("Error while reading control socket %d\n", errno);

		return;
	}

	if (ret < 4) {
		g_printerr ("Error control message to short\n");
		return;
	}
	buf[ret - 1] = 0;

	if (buf[0] == 's') {
		unsigned long long time;
		GstClockReturn clk_ret;

		if (data->playing == TRUE) {
			g_printerr ("Setting start while playing\n");
			return;
		}
		time = strtoll(&buf[2], NULL, 10) * 1000000;

		g_print ("\nSetting start time to %llu\n", time);

		data->clk_id = gst_clock_new_single_shot_id(data->clock, (time - data->time_diff) * GST_USECOND);

		clk_ret = gst_clock_id_wait_async (data->clk_id, clk_start_cb, data);
		if (clk_ret != GST_CLOCK_OK)
			g_printerr ("Clock doesn't work\n");
	} else if (buf[0] == 'e') {
		unsigned long long time;
		GstClockReturn clk_ret;

		if (data->playing == FALSE) {
			g_printerr ("Setting end while not playing\n");
			return;
		}
		time = strtoll(&buf[2], NULL, 10) * 1000000;

		g_print ("\nSetting end time to %llu\n", time);

		data->clk_id = gst_clock_new_single_shot_id(data->clock, (time - data->time_diff) * GST_USECOND);

		clk_ret = gst_clock_id_wait_async (data->clk_id, clk_end_cb, data);
		if (clk_ret != GST_CLOCK_OK)
			g_printerr ("Clock doesn't work\n");
	} else
		g_printerr ("Error control message doesn't start with a recognized letter\n");
}

int main(int argc, char *argv[]) {
	CustomData data;
	GstBus *bus;
	GstMessage *msg;
	GstClockTime time;
	struct timeval tv;

	if (argc < 2) {
		g_printerr ("Pass dst IP as parameter\n");

		return -1;
	}

	data.sock = open_control_connection();
	if (data.sock < 0) {
		g_printerr ("Open socket failed\n");
		return 1;
	}

	data.playing = FALSE;
	data.terminate = FALSE;
	data.duration = GST_CLOCK_TIME_NONE;

	/* Initialize GStreamer */
	gst_init (&argc, &argv);

	/* Create the elements */
	data.videosrc = gst_element_factory_make ("videotestsrc", "videotestsrc");
	data.timeoverlay = gst_element_factory_make ("timeoverlay", "timeoverlay");
	data.ffmpegcolorspace = gst_element_factory_make ("ffmpegcolorspace", "ffmpegcolorspace");
	data.x264enc = gst_element_factory_make ("x264enc", "x264enc");
	data.rtph264pay = gst_element_factory_make ("rtph264pay", "rtph264pay");
	data.udpsink = gst_element_factory_make ("udpsink", "udpsink");

	/* Create the empty pipeline */
	data.pipeline = gst_pipeline_new ("srv-pipeline");

	if (!data.pipeline || !data.videosrc || !data.timeoverlay || !data.ffmpegcolorspace ||
	    !data.x264enc || !data.rtph264pay || !data.udpsink) {
		g_printerr ("Not all elements could be created.\n");
		return -1;
	}

	gst_bin_add_many (GST_BIN (data.pipeline), data.videosrc, data.timeoverlay,
			  data.ffmpegcolorspace, data.x264enc, data.rtph264pay, data.udpsink, NULL);
	if (!gst_element_link (data.videosrc, data.timeoverlay)) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data.pipeline);
		return -1;
	}
	if (!gst_element_link (data.timeoverlay, data.ffmpegcolorspace)) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data.pipeline);
		return -1;
	}
	if (!gst_element_link (data.ffmpegcolorspace, data.x264enc)) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data.pipeline);
		return -1;
	}
	if (!gst_element_link (data.x264enc, data.rtph264pay)) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data.pipeline);
		return -1;
	}
	if (!gst_element_link (data.rtph264pay, data.udpsink)) {
		g_printerr ("Elements could not be linked.\n");
		gst_object_unref (data.pipeline);
		return -1;
	}

	/* Set the URI to play */
	g_object_set (data.udpsink, "host", argv[1], "port", 5000, NULL);

	data.clock = gst_system_clock_obtain ();
	time = gst_clock_get_time (data.clock);
	gettimeofday(&tv, NULL);
	data.time_diff = (unsigned long long)tv.tv_sec * (1000000ULL) + tv.tv_usec - GST_TIME_AS_USECONDS(time);
	time = gst_clock_get_time (data.clock);
	gettimeofday(&tv, NULL);
	data.time_diff += (unsigned long long)tv.tv_sec * (1000000ULL) + tv.tv_usec - GST_TIME_AS_USECONDS(time);
	time = gst_clock_get_time (data.clock);
	gettimeofday(&tv, NULL);
	data.time_diff += (unsigned long long)tv.tv_sec * (1000000ULL) + tv.tv_usec - GST_TIME_AS_USECONDS(time);
	time = gst_clock_get_time (data.clock);
	gettimeofday(&tv, NULL);
	data.time_diff += (unsigned long long)tv.tv_sec * (1000000ULL) + tv.tv_usec - GST_TIME_AS_USECONDS(time);
	data.time_diff /= 4;
	g_print ("Time: \t\t%lu\n", GST_TIME_AS_USECONDS(time));
	g_print ("Sys time: \t%llu\n", (unsigned long long)tv.tv_sec * (1000000ULL) + tv.tv_usec);
	g_print ("Diff: \t\t%llu\n", data.time_diff);

	/* Listen to the bus */
	bus = gst_element_get_bus (data.pipeline);
	do {
		serve_controller(&data);

		msg = gst_bus_timed_pop_filtered (bus, 100 * GST_MSECOND,
						  GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS | GST_MESSAGE_DURATION);

		/* Parse message */
		if (msg != NULL) {
			handle_message (&data, msg);
		} else {
			/* We got no message, this means the timeout expired */
			if (data.playing) {
				GstFormat fmt = GST_FORMAT_TIME;
				gint64 current = -1;

				/* Query the current position of the stream */
				if (!gst_element_query_position (data.pipeline, &fmt, &current)) {
					g_printerr ("Could not query current position.\n");
				}

				/* If we didn't know it yet, query the stream duration */
				if (!GST_CLOCK_TIME_IS_VALID (data.duration)) {
					if (!gst_element_query_duration (data.pipeline, &fmt, &data.duration)) {
						g_printerr ("Could not query current duration.\n");
					}
				}

				/* Print current position and total duration */
				g_print ("Position %" GST_TIME_FORMAT " / %" GST_TIME_FORMAT "\r",
					 GST_TIME_ARGS (current), GST_TIME_ARGS (data.duration));
			}
		}
	} while (!data.terminate);

	/* Free resources */
	close (data.sock);
	gst_object_unref (bus);
	gst_element_set_state (data.pipeline, GST_STATE_NULL);
	gst_object_unref (data.pipeline);
	return 0;
}

static void handle_message (CustomData *data, GstMessage *msg) {
	GError *err;
	gchar *debug_info;

	switch (GST_MESSAGE_TYPE (msg)) {
	case GST_MESSAGE_ERROR:
		gst_message_parse_error (msg, &err, &debug_info);
		g_printerr ("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
		g_printerr ("Debugging information: %s\n", debug_info ? debug_info : "none");
		g_clear_error (&err);
		g_free (debug_info);
		data->terminate = TRUE;
		break;
	case GST_MESSAGE_EOS:
		g_print ("End-Of-Stream reached.\n");
		data->terminate = TRUE;
		break;
	case GST_MESSAGE_DURATION:
		/* The duration has changed, mark the current one as invalid */
		data->duration = GST_CLOCK_TIME_NONE;
		break;
	case GST_MESSAGE_STATE_CHANGED: {
		GstState old_state, new_state, pending_state;
		gst_message_parse_state_changed (msg, &old_state, &new_state, &pending_state);
		if (GST_MESSAGE_SRC (msg) == GST_OBJECT (data->pipeline)) {
			g_print ("Pipeline state changed from %s to %s:\n",
				 gst_element_state_get_name (old_state), gst_element_state_get_name (new_state));

			/* Remember whether we are in the PLAYING state or not */
			data->playing = (new_state == GST_STATE_PLAYING);
		}
	} break;
	default:
		/* We should not reach here */
		g_printerr ("Unexpected message received.\n");
		break;
	}
	gst_message_unref (msg);
}
