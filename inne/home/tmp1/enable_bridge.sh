#!/bin/bash

ifconfig p6p1 0.0.0.0 promisc up

systemctl start openvswitch.service

# wait ovs up
sleep 5
ovs-vsctl del-port br-int ap

ifconfig br-int 10.1.1.201/24 up
ip r add default via 10.1.1.16
ovs-vsctl set-controller br-int tcp:0.0.0.0:6633
# wait stable
sleep 15
ovs-vsctl set-controller br-int tcp:10.1.1.16:6633
