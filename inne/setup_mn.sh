#!/bin/bash

  ip link add name komp-s1 type veth peer name s1-komp
  ip a add 10.5.1.1/8 dev komp-s1
  ip link set dev komp-s1 up
  ip link set dev s1-komp up
  ovs-vsctl add-port s1 s1-komp
  ip a fl eth0
  ip a add 192.168.99.5/24 dev eth0

  for vlan in `seq 101 104`
  do
      vconfig add eth0 $vlan
      ifconfig eth0.$vlan up
  done

  ovs-vsctl add-port s1 eth0.101
  ovs-vsctl add-port s2 eth0.102
  ovs-vsctl add-port s5 eth0.103
  ovs-vsctl add-port s6 eth0.104

  iptables -F
