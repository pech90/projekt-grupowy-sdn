"""Struktura nr 1

     u1----s1      s2
             \     / \
              \   /   \
                s3-----s4-----u2
               /  \
              /    \
             s5-----s6
            /  \      \ 
           u3   u4     u5

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
import random

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        n_sw = 10
        n_usr = 5
        switches = []
        users = []

        for i in range(n_sw):
            switches.append(self.addSwitch('s' + str(i+1)))

        for i in range(n_usr):
            users.append(self.addHost('u' + str(i+1)))

        # Add links
        for i in range(n_usr):
            self.addLink (users[i], switches[random.randint(0,n_sw-1)])

        for i in range(n_sw - 1):
            self.addLink (switches[i], switches[i+1])

        for n in range(n_sw * 2):
            i = random.randint(0,n_sw-1)
            j = random.randint(0,n_sw-1)
            while i == j:
                j = random.randint(0,n_sw-1)
            self.addLink (switches[i], switches[j])

topos = { 'mytopo': ( lambda: MyTopo() ) }
