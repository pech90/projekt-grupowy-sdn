#!/usr/bin/python2.7

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.node import RemoteController
import mininet
import random

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        n_sw = 30
        n_usr = 20
        switches = []
        users = []

        for i in range(n_sw):
            switches.append(self.addSwitch('s' + str(i+1)))

        for i in range(n_usr):
            users.append(self.addHost('u' + str(i+1)))
        
        # Add links
        for i in range(n_usr):
            self.addLink (users[i], switches[random.randint(0,n_sw-1)])

        for i in range(n_sw - 1):
            self.addLink (switches[i], switches[i+1])

        for n in range(n_sw * 2):
            i = random.randint(0,n_sw-1)
            j = random.randint(0,n_sw-1)
            while i == j:
                j = random.randint(0,n_sw-1)
            self.addLink (switches[i], switches[j])

net = Mininet(topo=MyTopo(),controller=RemoteController,autoSetMacs=True,autoStaticArp=True)

for h in net.hosts:
    h.cmd("sleep 60 && while true; do echo -n 'Hello!' | nc -4u -q1 10.5.1.1 60000; sleep 1; done &")

net.start()
CLI(net)
net.stop()
